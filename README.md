<h1 align="center">Input Component</h1>

<div align="center">
   Solution for a challenge from  <a href="http://devchallenges.io" target="_blank">Devchallenges.io</a>.
</div>

<div align="center">
  <h3>
    <a href="https://islambeg-frontend-projects.gitlab.io/frameworks-land/input-component/">
      Demo
    </a>
    <span> | </span>
    <a href="https://gitlab.com/islambeg-frontend-projects/frameworks-land/input-component">
      Solution
    </a>
    <span> | </span>
    <a href="https://devchallenges.io/challenges/TSqutYM4c5WtluM7QzGp">
      Challenge
    </a>
  </h3>
</div>

## Table of Contents

- [Overview](#overview)
  - [Built With](#built-with)
- [Features](#features)
- [How to use](#how-to-use)
- [Acknowledgements](#acknowledgements)
- [Contact](#contact)

## Overview

This project is build with Vue framework. I chose it in order to try it out.
As it turns out working with Vue is quite a pleasant experience.
It also has good Typescript support (besides a few quirks here and there)
so it was nice to have things type checked during development.

### Built With

- [Vue.js](https://vuejs.org/)
- [Typescript](https://www.typescriptlang.org/)
- [Sass](https://sass-lang.com/)
- [Vite](vitejs.dev/)

## Features

- Input and textarea fields wrapped in a single interface
- Fully typed (props are passed as an object in order to have better typing support)
- Label as text field or custom slot input
- Placeholder as text field or custom slot input
- Icons as Google Material Icons
- Seemless interaction with native input/textarea elements
- A11y support

## How To Use

To clone and run this application, you'll need [Git](https://git-scm.com) and [Node.js](https://nodejs.org/en/download/) (which comes with [npm](http://npmjs.com)) installed on your computer. From your command line:

```bash
# Clone this repository
$ git clone https://gitlab.com/islambeg-frontend-projects/frameworks-land/input-component

# Install dependencies
$ npm install

# Start dev server
$ npm run dev
```

## Acknowledgements

- [Material Icons by Google](https://material.io/resources/icons/?style=round)
- [Nano ID](https://github.com/ai/nanoid)

## Contact

- Email islambeg@proton.me
- Gitlab [@islambeg](https://gitlab.com/islambeg)
