import type { InputHTMLAttributes, TextareaHTMLAttributes } from "vue";
import type { MaterialIconName } from "./MaterialIcon";

interface CommonProps {
  id: string;
  error?: boolean;
  disabled?: boolean;
  modelValue?: string;
  placeholder?: string;
  labelText?: string;
  helperText?: string;
  ariaDescribedBy?: string;
  startIcon?: MaterialIconName;
  endIcon?: MaterialIconName;
  fullwidth?: boolean;
}

type commonPropsToOmit =
  | "id"
  | "placeholder"
  | "value"
  | "onInput"
  | "disabled";

export interface InputProps extends CommonProps {
  small?: boolean;
  multiline?: false;
  nativeInputProps?: Omit<InputHTMLAttributes, commonPropsToOmit>;
}

export interface TextareaProps extends CommonProps {
  multiline: true;
  rows?: number;
  nativeTextareaProps?: Omit<TextareaHTMLAttributes, commonPropsToOmit>;
}
