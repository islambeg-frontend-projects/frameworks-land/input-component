<script setup lang="ts">
import { ref, computed, onMounted } from "vue";
import type { PropType } from "vue";
import { nanoid } from "nanoid";

import MaterialIcon from "./MaterialIcon.vue";
import type { InputProps, TextareaProps } from "./LabeledInput";

const props = defineProps({
  attributes: {
    type: Object as PropType<InputProps | TextareaProps>,
    required: true,
  },
});

const emit = defineEmits<{
  (e: "update:modelValue", value: string): void;
}>();

const value = computed({
  get(): string {
    return props.attributes.modelValue || "";
  },
  set(value: string) {
    emit("update:modelValue", value);
  },
});

const input = ref<HTMLInputElement | null>(null);
const helperDefault = ref<HTMLParagraphElement | null>(null);
defineExpose({ input });

onMounted(() => {
  if (!props.attributes.helperText || !props.attributes.helperText) {
    return;
  }

  if (!input.value) {
    return;
  }

  const ariaDescribedBy = props.attributes.ariaDescribedBy || nanoid();
  input.value.setAttribute("aria-describedby", ariaDescribedBy);
  helperDefault.value &&
    helperDefault.value.setAttribute("id", ariaDescribedBy);
});
</script>

<template>
  <section
    :class="[
      'input-field',
      {
        error: props.attributes.error,
        disabled: props.attributes.disabled,
        multiline: props.attributes.multiline,
        fullwidth: props.attributes.fullwidth,
        'with-start-icon': props.attributes.startIcon,
        'with-end-icon': props.attributes.endIcon,
      },
    ]"
  >
    <label :for="props.attributes.id" class="label">
      <section
        v-if="props.attributes.labelText == undefined"
        class="label__content"
      >
        <slot></slot>
      </section>
      <p v-else class="label__content">{{ props.attributes.labelText }}</p>
    </label>
    <MaterialIcon
      v-if="props.attributes.startIcon"
      :name="props.attributes.startIcon"
      class="icon icon--start"
    />
    <input
      v-if="!props.attributes.multiline"
      :id="props.attributes.id"
      :placeholder="props.attributes.placeholder"
      v-model="value"
      v-bind="props.attributes.nativeInputProps"
      :class="[
        'input',
        {
          'input--sm': props.attributes.small,
        },
      ]"
      ref="input"
      :disabled="props.attributes.disabled"
    />
    <textarea
      v-else
      :id="props.attributes.id"
      v-model="value"
      v-bind="props.attributes.nativeTextareaProps"
      :placeholder="props.attributes.placeholder"
      :class="['input']"
      ref="input"
      :disabled="props.attributes.disabled"
      :rows="
        props.attributes.rows && props.attributes.rows < 1
          ? 1
          : props.attributes.rows
      "
    />
    <MaterialIcon
      v-if="props.attributes.endIcon"
      :name="props.attributes.endIcon"
      class="icon icon--end"
    />
    <slot v-if="props.attributes.helperText == undefined" name="helper"></slot>
    <p
      v-else-if="props.attributes.helperText"
      class="helper"
      ref="helperDefault"
    >
      {{ props.attributes.helperText }}
    </p>
  </section>
</template>

<style scoped lang="scss">
$field-width: 20rem;
$padding--block--md: var(--labeled-input--padding--block--md, 1.7rem);
$padding--block--sm: var(--labeled-input--padding--block--sm, 0.9rem);
$line-height: var(--labeled-input--line-height, 1.45);
$color--background: var(--labeled-input--color--border, transparent);
$color--background--disabled: var(--labeled-input--color--border, #e0e0e0);
$color--border: var(--labeled-input--color--border, #828282);
$color--border--hover: var(--labeled-input--color--border--hover, #333);
$color--border--focus: var(--labeled-input--color--border--focus, #2962ff);
$color--error: var(--labeled-input--color--error, #d32f2f);
$color--text--primary: var(--labeled-input--color--text--primary, #333);
$color--text--secondary: var(--labeled-input--color--text--secondary, #828282);
$transition--duration: 150ms;
$transition--timing-function: ease-in-out;
$icon--size: var(--labeled-input--icon--size, 1.5em);

* {
  box-sizing: border-box;
}

.label {
  color: $color--text--primary;
  font-size: 0.85em;
  transition: text-decoration $transition--duration $transition--timing-function,
    color text-decoration $transition--duration $transition--timing-function;
}

.input {
  caret-color: $color--text--primary;
  width: $field-width;
  padding: $padding--block--md 1.2rem;
  font: inherit;
  border-radius: 0.8rem;
  transition: border $transition--duration $transition--timing-function,
    outline $transition--duration $transition--timing-function;
  border: 0.1rem solid $color--border;
  outline: 0.1rem solid transparent;
  background-color: $color--background;
  grid-column: 1 / -1;
  grid-row: 2 / span 1;

  &::placeholder {
    opacity: 1;
    color: $color--text--secondary;
  }

  &--sm {
    padding-block-start: $padding--block--sm;
    padding-block-end: $padding--block--sm;
  }
}

.icon {
  font-size: $icon--size;
  color: $color--text--secondary;
  grid-column: 1 / -1;
  grid-row: 2 / span 1;
  align-self: center;
  width: max-content;

  &--start {
    margin-left: 0.8rem;
  }

  &--end {
    justify-self: end;
    margin-right: 0.8rem;
  }
}

.helper {
  font-size: 0.7em;
  transition: color text-decoration $transition--duration
    $transition--timing-function;
}

.error {
  & .label,
  .helper {
    color: $color--error;
  }

  .input {
    border-color: $color--error;
  }
}

.input-field {
  line-height: $line-height;
  display: grid;
  grid-template-columns: $field-width;
  row-gap: 0.4rem;

  &.with-start-icon {
    & .input {
      padding-left: calc(0.8rem + $icon--size + 0.8rem);
    }
  }

  &.with-end-icon {
    & .input {
      padding-right: calc(0.8rem + $icon--size + 0.8rem);
    }
  }

  &.fullwidth {
    grid-template-columns: 100%;

    & .input {
      width: 100%;
    }
  }

  &.disabled {
    .input {
      cursor: not-allowed;
      background-color: $color--background--disabled;
      transition: none;
    }
  }

  &:not(.disabled) {
    &:hover {
      & .input {
        border: 0.1rem solid $color--border--hover;
        outline: 0.1rem solid $color--border--hover;
      }

      &.error {
        & .label,
        .helper {
          color: $color--text--primary;
        }
      }
    }

    &:focus-within {
      & .input {
        border: 0.1rem solid $color--border--focus;
        outline: 0.15rem solid $color--border--focus;
      }

      &.error {
        & .label {
          text-decoration: underline;
          text-decoration-thickness: 0.2rem;
        }

        & .input {
          border: 0.1rem solid $color--error;
          outline: 0.15rem solid $color--error;
        }
      }
    }
  }
}
</style>
